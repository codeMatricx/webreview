<footer class="-footer" id="footer">
    <div class="container">
        <div class="footer-menu">
            <div class="-menu">
                
                                                <nav class="-nav">
                            <h5 class="-title">Resources</h5>
                            <ul class="-list">
                                                                
                                                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-seo-guides"
                                                                                title="SEO Guides">
                                            SEO Guides                                    </a>
                                </li>
                                                               
                                                            </ul>
                        </nav>
                                               
                                                <nav class="-nav">
                            <h5 class="-title">Company</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-about"
                                                                                title="About Us">
                                            About Us                                    </a>
                                </li>
                                                
                                                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-privacy"
                                                                                title="Privacy">
                                            Privacy                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-tos"
                                                                                title="Terms of Service">
                                            Terms of Service                                    </a>
                                </li>
                                                            </ul>
                        </nav>
                                                <nav class="-nav">
                            <h5 class="-title">Support</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-faq"
                                                                                title="Need Help?">
                                            Need Help?                                    </a>
                                </li>
                                                            </ul>
                        </nav>
                        <nav class="-nav">
                            <h5 class="-title">Contact Us</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-faq"
                                                                                title="Need Help?">
                                            Call us: +1-8455104                                   </a>
                                </li>
                                <li class="-item ">
                                    <a href="#"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-faq"
                                                                                title="Need Help?">
                                            Email:-Carrer@gmail.com                                  </a>
                                </li>
                                                            </ul>
                        </nav>
                            </div>
            <div class="-social">
                <ul class="-list">
                                        <li class="-item">
                        <a href="https://www.facebook.com/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-facebook"
                          title="Facebook">
                                                <svg class="icon icon-facebook-nocolor" role="img" style="">
                <use xlink:href="#facebook-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://twitter.com/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-twitter"
                          title="Twitter">
                                                <svg class="icon icon-twitter-nocolor" role="img" style="">
                <use xlink:href="#twitter-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://www.linkedin.com/company/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-linkedin"
                          title="LinkedIn">
                                                <svg class="icon icon-linkedin-nocolor" role="img" style="">
                <use xlink:href="#linkedin-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://www.youtube.com/user/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-youtube"
                          title="YouTube">
                                                <svg class="icon icon-youtube-nocolor" role="img" style="">
                <use xlink:href="#youtube-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://www.instagram.com/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-instagram"
                          title="Instagram">
                                                <svg class="icon icon-instagram-nocolor" role="img" style="">
                <use xlink:href="#instagram-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://plus.google.com/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-googleplus"
                          title="Google+">
                                                <svg class="icon icon-googleplus-nocolor" role="img" style="">
                <use xlink:href="#googleplus-nocolor"></use>
            </svg>                        </a>
                    </li>
                                    </ul>
            </div>
        </div>
        <div class="footer-copyright">
          <div class="brand">
              <!-- <a class="-logo"
                  href="index.html"
                  data-ga-label="footer-woorank-logo"
                  title="WooRank.com | SEO Checker - Website Review">
                                <svg class="icon icon-logo" role="img" style="">
                <use xlink:href="#logo"></use>
            </svg>              </a> -->
              <span class="sr-only">Copyright &copy; 2018</span>
          </div>
         
        </div>
    </div>
</footer>