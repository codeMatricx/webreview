<header class="header navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Navigation mobile trigger button -->
                <button type="button"
                    class="navbar-toggle collapsed visible-xs"
                    data-toggle="collapse"
                    data-target="#top-nav-collapse">
                                <svg class="icon icon-menu fill-grey" role="img" style="">
                <use xlink:href="#menu"></use>
            </svg>                </button>

                
                <a class=""
                    href="../../index.php"
                    title="webReview.com | SEO Checker - Website Review" style="text-decoration: none;">
                    <span class="sr-only">webReview.com | SEO Checker - Website Review</span>
                                <!-- <svg class="icon icon-logo" role="img" style=""> -->
                                   <!--  <img class="" src="assets/img/new_image/imageedit_1_6114920061.png" style="height: 60px; margin-left: -60px; "> -->
                                   <h3 style="margin-top: 13px; margin-left: -60px; "><span style="color: #b55858;">Web</span><span style="color: #76b5ea;">Review</span></h3>
                <use xlink:href="#logo"></use>
            </svg>                </a>

            </div>

            <div class="navbar-collapse collapse" id="top-nav-collapse" aria-expanded="false">
                <ul class="nav navbar-nav">
                                    
                     <li>
                                    <a href="en/features/advanced-seo-audit.php"
                                        class="gtm-feat-dropdown-feature_advanced_seo_audit">
                                        Advanced SEO Audit                                    </a>
                                </li>

                                                            <li>
                                    <a href="en/features/keyword-tool.php"
                                        class="gtm-feat-dropdown-feature_keyword_tool">
                                        Keyword Tool                                    </a>
                                </li>
                    <li>
                                    <a href="en/features/site-crawl.php"
                                        class="gtm-feat-dropdown-feature_site_crawl">
                                        Site Crawl                                    </a>
                                </li>
                     <li>
                                    <a href="en/features/seo-monitoring.php"
                                        class="gtm-feat-dropdown-feature_seo_monitoring">
                                        SEO Monitoring                                    </a>
                                </li>
                     <li>
                                    <a href="en/features/marketing-checklist.php"
                                        class="gtm-feat-dropdown-feature_marketing_checklist">
                                        Digital Marketing Checklist                                    </a>
                                </li>
                          </ul>

                
            </div>
        </div>
    </header>