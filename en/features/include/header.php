<header class="header navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Navigation mobile trigger button -->
                <button type="button"
                    class="navbar-toggle collapsed visible-xs"
                    data-toggle="collapse"
                    data-target="#top-nav-collapse">
                                <svg class="icon icon-menu fill-grey" role="img" style="">
                <use xlink:href="#menu"></use>
            </svg>                </button>

                
                <a class=""
                    href="../../index.php"
                    title="webReview.com | SEO Checker - Website Review">
                    <span class="sr-only">webReview.com | SEO Checker - Website Review</span>
                                <!-- <svg class="icon icon-logo" role="img" style=""> -->
                                    <img class="" src="include/imageedit_1_6114920061.png" style="height: 60px;">
                <use xlink:href="#logo"></use>
            </svg>                </a>

            </div>

            <div class="navbar-collapse collapse" id="top-nav-collapse" aria-expanded="false">
                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            Features                                        <svg class="icon icon-chevron-down" role="img" style="">
                <use xlink:href="#chevron-down"></use>
            </svg>                        </a>

                        <ul class="dropdown-menu js-feat-dropdown" role="menu">
                                                            <li>
                                    <a href="advanced-seo-audit.php"
                                        class="gtm-feat-dropdown-feature_advanced_seo_audit">
                                        Advanced SEO Audit                                    </a>
                                </li>
                                                            <li>
                                    <a href="keyword-tool.php"
                                        class="gtm-feat-dropdown-feature_keyword_tool">
                                        Keyword Tool                                    </a>
                                </li>
                                                            <li>
                                    <a href="site-crawl.php"
                                        class="gtm-feat-dropdown-feature_site_crawl">
                                        Site Crawl                                    </a>
                                </li>
                                                            <li>
                                    <a href="seo-monitoring.php"
                                        class="gtm-feat-dropdown-feature_seo_monitoring">
                                        SEO Monitoring                                    </a>
                                </li>
                                                            <li>
                                    <a href="marketing-checklist.php"
                                        class="gtm-feat-dropdown-feature_marketing_checklist">
                                        Digital Marketing Checklist                                    </a>
                                </li>
                                                    </ul>

                    </li>
                    <li>
                        <a href="../p/pricing.php" class="js-pricing-link gtm-pricing-navbar-link">
                            Pricing                        </a>
                    </li>
                                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li id="login-container">
                        <a class="navbar-btn btn-default link" href="#">
                            Log In        </a>
                    </li>
    <!-- <li id="signup-container">
        <a class="navbar-btn btn-warning js-signup-page" href="en/register/plan/index.html">
            Free Trial        
        </a>
    </li> -->
                </ul>
            </div>
        </div>
    </header>