<footer class="-footer" id="footer">
    <div class="container">
        <div class="footer-menu">
            <div class="-menu">
                
                                                <nav class="-nav">
                            <h5 class="-title">Resources</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="en/blog.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-blog"
                                                                                title="Blog">
                                            Blog                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="en/edu/seo-guides.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-seo-guides"
                                                                                title="SEO Guides">
                                            SEO Guides                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="https://www.youtube.com/user/WooRankTV"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-youtube"
                                                                                title="Videos">
                                            Videos                                    </a>
                                </li>
                                                            </ul>
                        </nav>
                                                <nav class="-nav">
                            <h5 class="-title">WooRank</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="en/p/pricing.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-pricing"
                                                                                title="Pricing">
                                            Pricing                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="https://experts.woorank.com/en/experts"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-experts"
                                                                                title="Experts">
                                            Experts                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="https://index.woorank.com/"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-index"
                                                                                title="Review Index">
                                            Review Index                                    </a>
                                </li>
                                                                <li class="-item hidden">
                                    <a href="https://chrome.google.com/webstore/detail/seo-website-analysis/hlngmmdolgbdnnimbmblfhhndibdipaf"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-extension"
                                                                                  class="extension-link"
                                          data-url-firefox="https://addons.mozilla.org/en-us/firefox/addon/seo-website-analysis/"
                                                                                title="Extension">
                                            Extension                                    </a>
                                </li>
                                                            </ul>
                        </nav>
                        
                
                                                <nav class="-nav">
                            <h5 class="-title">Company</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="en/p/about.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-about"
                                                                                title="About Us">
                                            About Us                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="en/jobs.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-jobs"
                                                                                title="Jobs">
                                            Jobs                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="en/p/privacy.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-privacy"
                                                                                title="Privacy">
                                            Privacy                                    </a>
                                </li>
                                                                <li class="-item ">
                                    <a href="en/p/tos.html"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-tos"
                                                                                title="Terms of Service">
                                            Terms of Service                                    </a>
                                </li>
                                                            </ul>
                        </nav>
                                                <nav class="-nav">
                            <h5 class="-title">Support</h5>
                            <ul class="-list">
                                                                <li class="-item ">
                                    <a href="https://help.woorank.com/hc/en-us"
                                        rel="noopener noreferrer"
                                        data-ga-label="footer-link-faq"
                                                                                title="Need Help?">
                                            Need Help?                                    </a>
                                </li>
                                                            </ul>
                        </nav>
                        
                            </div>
            <div class="-social">
                <ul class="-list">
                                        <li class="-item">
                        <a href="https://www.facebook.com/woorank/"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-facebook"
                          title="Facebook">
                                                <svg class="icon icon-facebook-nocolor" role="img" style="">
                <use xlink:href="#facebook-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://twitter.com/woorank"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-twitter"
                          title="Twitter">
                                                <svg class="icon icon-twitter-nocolor" role="img" style="">
                <use xlink:href="#twitter-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://www.linkedin.com/company/woorank"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-linkedin"
                          title="LinkedIn">
                                                <svg class="icon icon-linkedin-nocolor" role="img" style="">
                <use xlink:href="#linkedin-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://www.youtube.com/user/WooRankTV"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-youtube"
                          title="YouTube">
                                                <svg class="icon icon-youtube-nocolor" role="img" style="">
                <use xlink:href="#youtube-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://www.instagram.com/woorank/?hl=en"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-instagram"
                          title="Instagram">
                                                <svg class="icon icon-instagram-nocolor" role="img" style="">
                <use xlink:href="#instagram-nocolor"></use>
            </svg>                        </a>
                    </li>
                                        <li class="-item">
                        <a href="https://plus.google.com/+woorank"
                          class="-circle"
                          rel="noopener noreferrer"
                          data-ga-label="footer-sharing-googleplus"
                          title="Google+">
                                                <svg class="icon icon-googleplus-nocolor" role="img" style="">
                <use xlink:href="#googleplus-nocolor"></use>
            </svg>                        </a>
                    </li>
                                    </ul>
            </div>
        </div>
        <div class="footer-copyright">
          <div class="brand">
              <!-- <a class="-logo"
                  href="index.html"
                  data-ga-label="footer-woorank-logo"
                  title="WooRank.com | SEO Checker - Website Review">
                                <svg class="icon icon-logo" role="img" style="">
                <use xlink:href="#logo"></use>
            </svg>              </a> -->
              <span class="sr-only">Copyright &copy; 2018</span>
          </div>
         
        </div>
    </div>
</footer>